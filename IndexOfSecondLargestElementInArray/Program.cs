﻿namespace IndexOfSecondLargestElementInArray;

public class Program
{
    static void Main(string[] args)
    {
        var result = IndexOfSecondLargestElementInArray(new int[] { 1 });
        Console.WriteLine(result);
        Console.ReadLine();
    }
    static int index;
    public static int IndexOfSecondLargestElementInArray(int[] x)
    {
        x = new int[] { 5 };
        if (x.Length <= 1)
        {
            return -1;
        }
        int secondMax;
        // kết quả hàm sau khi thực thi là vị trí(index) của số lớn thứ 2 trong mảng
        if (x[0] > x[1])
        {
            secondMax = x[1];
            index = 1;
        }
        else
        {
            secondMax = x[0];
        }
        for (int i = 2; i < x.Length; i++)
        {
            if (x[i] > secondMax)
            {
                secondMax = x[i];
                index = i;
            }
        }
        return index;
    }
}