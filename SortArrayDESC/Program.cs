﻿using System.ComponentModel.Design.Serialization;

namespace SortArrayDESC;

public class Program
{
    static void Main(string[] args)
    {
        var sorted = SortArrayDesc(new int[] { 1 });
        Console.Write("[");
        for (int i = 0; i < sorted.Length; i++)
        {
            if (i < sorted.Length - 1)
                Console.Write(sorted[i] + ", ");
            else
                Console.Write(sorted[i]);
        }
        Console.WriteLine("]");
        Console.ReadLine();
    }

    public static int[] SortArrayDesc(int[] x)
    {
        //kết quả hàm sau khi thực thi là mảng số nguyên mới đã sắp xếp theo thứ tự giảm dần
        x = new int[] { 4, 2, 3, 4 };
        Array.Sort<int>(x, delegate (int m, int n) { return n - m; });


        //foreach (int value in x)
        //{
        //    Console.Write(value + ", ");
        ////}
        //Console.WriteLine("]");

        return x;
    }

}