using FluentAssertions;

namespace Practice;

public class MixTwoStringsTests
{
    [Fact]
    public void MixTwoStrings_ShouldReturnEmpty_WhenBothParamIsEmpty()
    {
        // arrange
        var param = string.Empty;
        var param1 = string.Empty;
        var expected = string.Empty;

        // act
        var actual = PracticeStrings.MixTwoStrings(param, param1);

        // assert
        actual.Should().Be(expected);
    }

    [Fact]
    public void MixTwoStrings_ShouldReturnEmpty_WhenSecondParamIsEmpty()
    {
        // arrange
        var param = "Test";
        var param1 = string.Empty;
        var expected = "Test";

        // act
        var actual = PracticeStrings.MixTwoStrings(param, param1);

        // assert
        actual.Should().Be(expected);
    }

    [Fact]
    public void MixTwoStrings_ShouldReturnEmpty_WhenFirstParamIsEmpty()
    {
        // arrange
        var param = string.Empty;
        var param1 = "Test";
        var expected = "Test";

        // act
        var actual = PracticeStrings.MixTwoStrings(param, param1);

        // assert
        actual.Should().Be(expected);
    }

    [Fact]
    public void MixTwoStrings_ShouldReturnEmpty_WhenBothParamIsNotEmpty()
    {
        // arrange
        var param = "ccc";
        var param1 = "ddd";
        var expected = "cdcdcd";

        // act
        var actual = PracticeStrings.MixTwoStrings(param, param1);

        // assert
        actual.Should().Be(expected);
    }

    [Fact]
    public void MixTwoStrings_ShouldReturnEmpty_WhenBothParamIsNotSameLength()
    {
        // arrange
        var param = "ccc";
        var param1 = "dddeee";
        var expected = "cdcdcdeee";

        // act
        var actual = PracticeStrings.MixTwoStrings(param, param1);

        // assert
        actual.Should().Be(expected);
    }
}